#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/random.h>

struct birthday {
int day;
int month;
int year;
struct list_head list;
};

static LIST_HEAD(birthday_list);
struct birthday* person;

/* This function is called when the module is loaded. */
int simple_init(void)
{
       printk(KERN_INFO "Loading Module\n");


       int i;
       for(i=0; i<3; i++)
       {
              person = kmalloc(sizeof(*person), GFP_KERNEL);
              person->day = get_random_int();
              person->month = get_random_int();
              person->year = get_random_int();
              INIT_LIST_HEAD(&person->list);
              list_add_tail(&person->list, &birthday_list);
       }
       return 0;
}

/* This function is called when the module is removed. */
void simple_exit(void) {
	printk(KERN_INFO "Removing Module\n");

       struct birthday *ptr, *next;
       list_for_each_entry_safe(ptr,next,&birthday_list,list) {
              /* on each iteration ptr points */
              /* to the next birthday struct */
              printk(KERN_INFO "birthday %d-%d-%d\n", ptr->day, ptr->month, ptr->year);
              list_del(&ptr->list);
              kfree(ptr);
       }

	printk(KERN_INFO "Linked list removed\n");

}

/* Macros for registering module entry and exit points. */
module_init( simple_init );
module_exit( simple_exit );

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Simple Module");
MODULE_AUTHOR("SGG");
