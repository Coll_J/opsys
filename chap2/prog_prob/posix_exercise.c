#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char** argv)
{
    char* f_input = NULL;
    char* f_output = NULL;

    int c;

    while((c = getopt(argc, argv, "i:o:")) != -1)
    {
        switch(c)
        {
            case 'i':
                f_input = optarg;
                break;
            case 'o':
                f_output = optarg;
                break;
            default:
                break;
        }
    }

    char temp[100];
    if(f_input == NULL)
    {
        printf("enter input file:\n");
        scanf("%s", temp);
        f_input = temp;
    }
    
    if(f_output == NULL)
    {
        printf("enter output file:\n");
        scanf("%s", temp);
        f_output = temp;
    }

    printf("input= %s, output= %s\n", f_input, f_output);

    int fd = -1;
    fd = open(f_input, O_RDONLY);
    if(fd == -1)
    {
        printf("error open file!\n");
        return -1;
    }

    off_t size = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);

    char* buff = malloc(size + 1);
    read(fd, buff, size);

    printf("%s\n", buff);

    fd = open(f_output, O_CREAT | O_RDWR , S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if(fd == -1)
    {
        printf("error create file!\n");
        return -1;
    }
    write(fd, buff, size);

    return 0;
}